<?php

if (file_exists('../beckon.txt')) {
  $names = file('../beckon.txt');
  $names = array_reverse($names);
} else {
  $names = array();
}

$output = "<table class='beckon-table'>\n";
foreach ($names as $name) {
  list($time, $student) = preg_split('/\|/', $name);
  $output .= "<tr><td>" . date('Y-m-d H:i', $time) . "</td>";
  $output .= "<td>$student</td></tr>\n";
}
$output .= "</table>";

echo $output;
